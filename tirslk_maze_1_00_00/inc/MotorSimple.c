// MotorSimple.c
// Runs on MSP432
// Provide mid-level functions that initialize ports and
// set motor speeds to move the robot.
// Starter code for Lab 12, uses Systick software delay to create PWM
// Daniel Valvano
// July 7, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// Sever VCCMD=VREG jumper on Motor Driver and Power Distribution Board and connect VCCMD to 3.3V.
//   This makes P3.7 and P3.6 low power disables for motor drivers.  0 to sleep/stop.
// Sever nSLPL=nSLPR jumper.
//   This separates P3.7 and P3.6 allowing for independent control
// Left motor direction connected to P1.7 (J2.14)
// Left motor PWM connected to P2.7/TA0CCP4 (J4.40)
// Left motor enable connected to P3.7 (J4.31)
// Right motor direction connected to P1.6 (J2.15)
// Right motor PWM connected to P2.6/TA0CCP3 (J4.39)
// Right motor enable connected to P3.6 (J2.11)

#include <stdint.h>
#include "msp.h"
#include "../inc/SysTick.h"
#include "../inc/Bump.h"

// *******Lab 12 solution*******

void Motor_InitSimple(void){
// Initializes the 6 GPIO lines and puts driver to sleep
// Returns right away

    // 1.7 and 2.7 is for the left side
    // 1.6 and 2.6 are for the right side
    // "P3.6 = P3.7 = 1 to activate, not sleep" What does this mean

    // write this as part of Lab 12
    // Initialize P1.7 and P1.6 - PH DIRL/DIRR
    // The R and L direction
    P1->SEL0 &= ~0xc0;
    P1->SEL1 &= ~0xc0;
    P1->DIR |= 0xc0;
    P1->OUT &= ~0xc0;

    // Initialize P2.7 and P2.6 - EN PWML/PWMR
    // The R and L on or off
    P2->SEL0 &= ~0xc0;
    P2->SEL1 &= ~0xc0;
    P2->DIR |= 0xc0;
    P2->OUT &= ~0xc0;

    // Initialize P3.7 and P3.6 - nSLEEP nSLPL
    // The R and L sleep
    P3->SEL0 &= ~0xc0;
    P3->SEL1 &= ~0xc0;
    P3->DIR |= 0xc0;
    P3->OUT &= 0xc0; //sleep
}

void Motor_StopSimple(void){
// Stops both motors, puts driver to sleep
// Returns right away
  P1->OUT &= ~0xC0;   // stop signal to both motors PH DIR
  P2->OUT &= ~0xC0;   // off fir EN PWM
  P3->OUT &= ~0xC0;   // low current sleep mode. 1 for activate, 0 for not active
}

void Motor_ForwardSimple(uint16_t duty, uint32_t time){
// Drives both motors forward at duty (100 to 9900)
// Runs for time duration (units=10ms), and then stops
// Stop the motors and return if any bumper switch is active
// Returns after time*10ms or if a bumper switch is hit

    // write this as part of Lab 12

    // activate motors
    P3->OUT |= 0xc0;

    uint8_t bump = Bump_Read();
    int count = time; // cant be unsigned or the int will roll over to all 1's and the while loop won't exit
    while (bump == 0xED && count >= 0) { // only runs if no bumpers are pressed and there is still more cycles to run
        P1->OUT &= ~0xc0;  // L & R forward
        P2->OUT |= 0xc0;
        //Clock_Delay1us(duty); //clock not included, using the systick functions
        SysTick_Wait(48 * duty);

        // duty cycle off time, stop the motors
        P1->OUT &= ~0xc0;
        P2->OUT &= ~0xc0;
        //Clock_Delay1us(10000 - duty); // see above clock delay
        SysTick_Wait(48 * (10000 - duty));

        // loop maintenance
        count--;
        bump = Bump_Read();
    }
    // if the loop breaks, it is because the bump was triggered or count is < 0
    // theoretically, the motors should be off, but i'll turn them off again and then send them to sleep
    P1->OUT &= ~0xc0;
    P2->OUT &= ~0xc0;
    P3->OUT &= ~0xc0;


}
void Motor_BackwardSimple(uint16_t duty, uint32_t time){
// Drives both motors backward at duty (100 to 9900)
// Runs for time duration (units=10ms), and then stops
// Runs even if any bumper switch is active
// Returns after time*10ms

    // activate motors
    P3->OUT |= 0xc0;

    uint8_t bump = Bump_Read();
    int count = time;
    while (bump == 0xED && count >= 0) { // only runs if no bumpers are pressed and there is still more cycles to run
        // Backward!
        P1->OUT |= 0xc0;
        P2->OUT |= 0xc0;
        //Clock_Delay1us(duty); //clock not included, using the systick functions
        SysTick_Wait(48 * duty);

        // duty cycle off time, stop the motors
        P1->OUT &= ~0xc0;
        P2->OUT &= ~0xc0;
        //Clock_Delay1us(10000 - duty); // see above clock delay
        SysTick_Wait(48 * (10000 - duty));

        // loop maintenance
        count--;
        bump = Bump_Read();
    }
    // if the loop breaks, it is because the bump was triggered or count is < 0
    // theoretically, the motors should be off, but i'll turn them off again and then send them to sleep
    P1->OUT &= ~0xc0;
    P2->OUT &= ~0xc0;
    P3->OUT &= ~0xc0;


  // write this as part of Lab 12
}
void Motor_LeftSimple(uint16_t duty, uint32_t time){ // drives the robot right
// Drives just the left motor forward at duty (100 to 9900)
// Right motor is stopped (sleeping)
// Runs for time duration (units=10ms), and then stops
// Stop the motor and return if any bumper switch is active
// Returns after time*10ms or if a bumper switch is hit

  // write this as part of Lab 12

    // activate left motor
    P3->OUT |= 0x80;

    uint8_t bump = Bump_Read();
    int count = time;
    while (bump == 0xED && count >= 0) { // only runs if no bumpers are pressed and there is still more cycles to run
        // left motor forward
        P1->OUT &= ~0x80;
        P2->OUT |= 0x80;
        //Clock_Delay1us(duty); //clock not included, using the systick functions
        SysTick_Wait(48 * duty);

        // duty cycle off time, stop the motors
        P1->OUT &= ~0x80;
        P2->OUT &= ~0x80;
        //Clock_Delay1us(10000 - duty); // see above clock delay
        SysTick_Wait(48 * (10000 - duty));

        // loop maintenance
        count--;
        bump = Bump_Read();
    }
    // if the loop breaks, it is because the bump was triggered or count is < 0
    // theoretically, the motors should be off, but i'll turn them off again and then send them to sleep
    P1->OUT &= ~0xc0;
    P2->OUT &= ~0xc0;
    P3->OUT &= ~0xc0;

}
void Motor_RightSimple(uint16_t duty, uint32_t time){ // drives the robot left
// Drives just the right motor forward at duty (100 to 9900)
// Left motor is stopped (sleeping)
// Runs for time duration (units=10ms), and then stops
// Stop the motor and return if any bumper switch is active
// Returns after time*10ms or if a bumper switch is hit

  // write this as part of Lab 12

    // activate right motor
    P3->OUT |= 0x40;

    uint8_t bump = Bump_Read();
    int count = time;
    while (bump == 0xED && count >= 0) { // only runs if no bumpers are pressed and there is still more cycles to run
        // right motor forward
        P1->OUT &= ~0x40;
        P2->OUT |= 0x40;
        //Clock_Delay1us(duty); //clock not included, using the systick functions
        SysTick_Wait(48 * duty);

        // duty cycle off time, stop the motors
        P1->OUT &= ~0x40;
        P2->OUT &= ~0x40;
        //Clock_Delay1us(10000 - duty); // see above clock delay
        SysTick_Wait(48 * (10000 - duty));

        // loop maintenance
        count--;
        bump = Bump_Read();
    }
    // if the loop breaks, it is because the bump was triggered or count is < 0
    // theoretically, the motors should be off, but i'll turn them off again and then send them to sleep
    P1->OUT &= ~0xc0;
    P2->OUT &= ~0xc0;
    P3->OUT &= ~0xc0;
}
