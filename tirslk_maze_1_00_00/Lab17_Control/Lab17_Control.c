// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// September 12, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/FlashProgram.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"


/**************Initial values used for all programs******************/
//used 7000 and 3000 for part 1, 5000 and 1000 for part 2
#define PWMNOMINAL 5000
#define SWING 1000
#define PWMIN (PWMNOMINAL-SWING)
#define PWMAX (PWMNOMINAL+SWING)

volatile uint32_t ControllerFlag; // set every 10ms on controller execution

int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec

// proportional controller gain
// experimentally determine value that creates a stable system
// may want a different gain for each program
int32_t Kp1= 80;

/**************Functions used by all programs***********************/
void Pause3(void){
  int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // restart Jacki
  UR = UL = PWMNOMINAL;    // reset parameters
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}

/**************Program17_1******************************************/
#define DESIRED_SPEED 100
#define TACHBUFF 10                      // number of elements in tachometer array
#define UMAX 14998
#define UMIN 0

int32_t ActualSpeedL, ActualSpeedR;      // Actual speed
int32_t ErrorL, ErrorR;                  // X* - X'
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)

int i = 0;

void LCDClear1(void){
  Nokia5110_Init();
  Nokia5110_Clear();
  Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Error(RPM)  L     R     ");
}
void LCDOut1(void){
  Nokia5110_SetCursor(1, 1);         // one leading space, second row
  Nokia5110_OutSDec1(DESIRED_SPEED);
  Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
  Nokia5110_OutSDec1(DESIRED_SPEED);
  Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
  Nokia5110_OutUDec(ActualSpeedL);
  Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
  Nokia5110_OutUDec(ActualSpeedR);
  Nokia5110_SetCursor(1, 5);       // zero leading spaces, sixth row
  Nokia5110_OutSDec1(ErrorL);
  Nokia5110_SetCursor(7, 5);       // six leading spaces, sixth row
  Nokia5110_OutSDec1(ErrorR);
}

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;
  for(i=0; i<length; i=i+1){
    sum = sum + array[i];
  }
  return (sum/length);
}

//**************************************************
// Proportional controller to drive straight with no
// sensor input
int8_t bump;
void Controller1(void){

  if(Mode){
    // pull tachometer information
    Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);

    // pull bump sensor information
    bump = Bump_Read();

    i++;
    if (i >= TACHBUFF) {
      i = 0;
      // use average of ten tachometer values to determine
      // actual speed (similar to Lab16)
      ActualSpeedL = 2000000 / avg(LeftTach, TACHBUFF);
      ActualSpeedR = 2000000 / avg(RightTach, TACHBUFF);


      // use proportional control to update duty cycle
      // LeftDuty = LeftDuty + Kp*LeftError
      ErrorR = DESIRED_SPEED - ActualSpeedR; // x* - x'
      ErrorL = DESIRED_SPEED - ActualSpeedL; // x* - x'
      UR = UR + Kp1 * ErrorR;
      UL = UL + Kp1 * ErrorL;

      // check min/max duty values
      if (UR < UMIN) {
        UR = UMIN;
      }
      if (UR > UMAX) {
        UR = UMAX;
      }
      if (UL < UMIN) {
        UL = UMIN;
      }
      if (UL > UMAX) {
        UL = UMAX;
      }

      // functionality added for level 3
      if(bump == 0x80 || bump == 0x40){ // hit left 2 most bumpers
        // turn right a little
        Motor_Stop();
        Motor_Backward(5000,5000);
        Clock_Delay1ms(100);

        Motor_Right(5000, 5000);
        Clock_Delay1ms(100);

      }else if(bump == 0x04 || bump == 0x01){ // hit right 2 most bumpers
        // turn left a little
        Motor_Stop();
        Motor_Backward(5000,5000);
        Clock_Delay1ms(100);

        Motor_Left(5000, 5000);
        Clock_Delay1ms(100);
      }else if(bump == 0x20 || bump == 0x08){ // hit one of the center bumpers
        // turn left 90 degrees
        bump = Bump_Read();
        Motor_Stop();
        Motor_Backward(5000,5000);
        Clock_Delay1ms(200);

        Motor_Left(5000, 5000);
        Clock_Delay1ms(400);
      }

      // update motor values
      Motor_Forward(UL, UR);
      ControllerFlag = 1;
    }
  }
}

// go straight with no sensor input
void Program17_1(void){
  DisableInterrupts();
  // initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Tachometer_Init();
  Motor_Init();

  // user TimerA1 to run the controller at 100 Hz
  TimerA1_Init(&Controller1, 500);

  Motor_Stop();
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  LCDClear1();
  ControllerFlag = 0;
  Pause3();
  while(1){
    if(ControllerFlag){
      LCDOut1();
      ControllerFlag = 0;
    }
  }
}

/**************Program17_2******************************************/
#define Kp2 9

volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm
volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosError;
int32_t Error;
int32_t ActualL, ActualR;

void LCDClear2(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("E= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
}

void LCDOut2(void){
  Nokia5110_SetCursor(3,2); Nokia5110_OutSDec(Left);
  Nokia5110_SetCursor(3,3); Nokia5110_OutSDec(Center);
  Nokia5110_SetCursor(3,4); Nokia5110_OutSDec(Right);
  Nokia5110_SetCursor(3,5); Nokia5110_OutSDec(Error);
  // left
  if(Time%5 == 0){
    UART0_OutUDec5(Left);UART0_OutString(" mm,");
    UART0_OutUDec5(Center);UART0_OutString(" mm,");
    UART0_OutUDec5(Right);UART0_OutString(" mm,");
    UART0_OutUDec5(UR);UART0_OutString(" %,");
    UART0_OutUDec5(UL);UART0_OutString(" %,");
    if(Error < 0){
      PosError = Error*(-1);
      UART0_OutString("-");UART0_OutUDec5(PosError);UART0_OutString("\n");
    }
    else{
      UART0_OutUDec5(Error);UART0_OutString("\n");
    }

  }
}

void IRsampling(void){
  uint32_t raw17, raw12, raw16;
  ADC_In17_12_16(&raw17, &raw12, &raw16);
  nr = LPF_Calc(raw17);
  nc = LPF_Calc2(raw12);
  nl = LPF_Calc3(raw16);
  Left = LeftConvert(nl);
  Center = CenterConvert(nc);
  Right = RightConvert(nr);
  ADCflag = 1;
}

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
void SysTick_Handler(void){
  if(Mode){
    // Determine set point
    if (Left > 172 && Right > 172) {
      SetPoint = (Left + Right) / 2;
    }
    else {
      SetPoint = 172;
    }

    // set error based off set point
    if (Left > Right) {
      Error = Left - SetPoint;
    }
    else if (Right > Left) {
      Error = SetPoint - Right;
    }

    // stop robot from hugging turns
    if(Left > 300){
      Error = SetPoint - Left;
    }

    // update duty cycle based on proportional control
    UR = UR + (Kp2 * Error);
    UL = UL - (Kp2 * Error);

    // check to ensure not too big of a swing
    if (UR < PWMIN) {
      UR = PWMIN;
    }
    if (UR > PWMAX) {
      UR = PWMAX;
    }
    if (UL < PWMIN) {
      UL = PWMIN;
    }
    if (UL > PWMAX) {
      UL = PWMAX;
    }

    // Turn left if close to a wall inn front
    if (Center < 130){
      Motor_Stop();
      Clock_Delay1ms(100);
      Motor_Backward(5000,5000);
      Clock_Delay1ms(200);

      Motor_Left(5000, 5000);
      Clock_Delay1ms(350);
    }

    // update motor values
    Motor_Forward(UL, UR);
    ControllerFlag = 1;
  }
}

// proportional control, wall distance
void Program17_2(void){
  uint32_t raw17,raw12,raw16;
  DisableInterrupts();
  // initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Motor_Init();

  // user TimerA1 to sample the IR sensors at 2000 Hz
  TimerA1_Init(&IRsampling, 250);

  Motor_Stop();
  LCDClear2();
  Mode = 0;
  UR = UL = PWMNOMINAL;
  ADCflag = ControllerFlag = 0;   // semaphores

  ADC0_InitSWTriggerCh17_12_16();   // initialize channels 17,12,16
  ADC_In17_12_16(&raw17,&raw12,&raw16);  // sample
  LPF_Init(raw17,64);     // P9.0/channel 17
  LPF_Init2(raw12,64);    // P4.1/channel 12
  LPF_Init3(raw16,64);    // P9.1/channel 16

  // user SysTick to run the controller at 100 Hz with a priority of 2
  SysTick_Init(480000, 2);

  Pause3();

  EnableInterrupts();
  while(1){
    if(Bump_Read()){ // collision
      Mode = 0;
      Motor_Stop();
      Pause3();
    }
    if(ControllerFlag){ // 100 Hz, not real time
      LCDOut2();
      ControllerFlag = 0;
    }
  }
}

/**************Program17_3******************************************/

uint8_t LineData;       // direct measure from line sensor
int32_t Position;       // position in 0.1mm relative to center of line
int32_t Turn;           // count the number of turns it makes after a bump
#define Kp3 9

void LCDClear3(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("Line Follow");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L =  "); Nokia5110_OutUDec(0);
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("P = "); Nokia5110_OutSDec(0);
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("UR=  "); Nokia5110_OutUDec(0);
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("UL=  "); Nokia5110_OutUDec(0);
}
void LCDOut3(void){
  Nokia5110_SetCursor(5,2); Nokia5110_OutUDec(LineData);
  Nokia5110_SetCursor(4,3); Nokia5110_OutSDec(Position);
  Nokia5110_SetCursor(5,4); Nokia5110_OutUDec(UR);
  Nokia5110_SetCursor(5,5); Nokia5110_OutUDec(UL);
}

/*
* Proportional controller to drive robot
* using line following
*/
void Controller3(void){
  Time++;

  // read values from line sensor, similar to
  // SysTick_Handler() in Lab10_Debugmain.c
  if(Time % 10 == 0){
    Reflectance_Start();    // Start Reflectance record every 10th time (every 10 ms)
  }
  if(Time % 10 == 9){
    LineData = ~Reflectance_End();     // end reflectance
  }

  // Use Reflectance_Position() to find position
  Position = Reflectance_Position(LineData);

  if(Mode){
    // update duty cycle based on porportional control
    // change is based on position only, no need to manually change it
    UR = PWMNOMINAL - Kp3 * Position;
    UL = PWMNOMINAL + Kp3 * Position;

    // limit change to within swing
    if(UR > PWMAX){
      UR = PWMAX;
    }
    if(UL > PWMAX){
      UL = PWMAX;
    }
    if(UR < PWMIN){
      UR = PWMIN;
    }
    if(UL < PWMIN){
      UL = PWMIN;
    }

    Motor_Forward(UL, UR);
    ControllerFlag = 1;
  }
}

// proportional control, line following
void Program17_3(void){
  DisableInterrupts();
  // initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Reflectance_Init();
  Motor_Init();

  // user TimerA1 to run the controller at 1000 Hz, this is every 0.0001s
  TimerA1_Init(&Controller3, 500);

  Motor_Stop();
  LCDClear3();
  Mode = 0;
  Turn = 0;
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  Pause3();
  while(1){
    if(Bump_Read()){ // collision
      Mode = 0;
      Motor_Stop();

      if(Turn == 0){ // ignore first collision
        Motor_Stop();

        Motor_Backward(5000,5000);
        Clock_Delay1ms(500);

        Motor_Right(5000, 5000);
        Clock_Delay1ms(800);
        Turn++;
        Mode = 1;
      }else if(Turn == 1){
        // set lights
        Clock_Delay1ms(200); LaunchPad_Output(0); // off
        Clock_Delay1ms(200); LaunchPad_Output(1); // red
        Clock_Delay1ms(100); LaunchPad_Output(0); // off
        Clock_Delay1ms(100); LaunchPad_Output(4); // blue
      }
    }
    if(ControllerFlag){ // 100 Hz , not real time
      LCDOut3();
      ControllerFlag = 0;
    }
  }
}

int32_t turnLv2 = 0;
void Controller4(void){
  Time++;

  // read values from line sensor, similar to
  // SysTick_Handler() in Lab10_Debugmain.c
  if(Time % 10 == 0){
    Reflectance_Start();    // Start Reflectance record every 10th time (every 10 ms)
  }
  if(Time % 10 == 9){
    LineData = Reflectance_End();     // end reflectance
  }

  // Use Reflectance_Position() to find position
  Position = Reflectance_Position(LineData);

  if(Mode){
    if(LineData == 0b11111111){     // turn Left at T section
      if(Turn == 4){ // at 5th T section stop (end of the maze)
        Motor_Stop();

        Mode = 0;
        Clock_Delay1ms(200); LaunchPad_Output(0); // off
        Clock_Delay1ms(200); LaunchPad_Output(1); // red
        Clock_Delay1ms(100); LaunchPad_Output(0); // off
        Clock_Delay1ms(100); LaunchPad_Output(4); // blue
        Pause3();
      }else{
        Motor_Stop();
        Clock_Delay1ms(100);
        Motor_Forward(5000,5000);
        Clock_Delay1ms(200);

        Motor_Left(5000, 5000);
        Clock_Delay1ms(400);
        Turn++;

        Mode = 1;
      }
    }
    if(LineData == 0b11111000){     // turn Left at left turns
      Motor_Stop();
      Clock_Delay1ms(100);
      Motor_Forward(5000,5000);
      Clock_Delay1ms(200);

      Motor_Left(5000, 5000);
      Clock_Delay1ms(400);

      Mode = 1;
    }

    // update duty cycle based on porportional control
    UR = PWMNOMINAL - Kp3 * Position;
    UL = PWMNOMINAL + Kp3 * Position;

    // limit change to within swing
    // change is based on position only, no need to manually change it
    if(UR > PWMAX){
      UR = PWMAX;
    }
    if(UL > PWMAX){
      UL = PWMAX;
    }
    if(UR < PWMIN){
      UR = PWMIN;
    }
    if(UL < PWMIN){
      UL = PWMIN;
    }

    Motor_Forward(UL, UR);
    ControllerFlag = 1;
  }
}

void Level_2(void){
  DisableInterrupts();
  // initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Reflectance_Init();
  Motor_Init();

  // user TimerA1 to run the controller at 1000 Hz
  TimerA1_Init(&Controller4, 500);

  Motor_Stop();
  LCDClear3();
  Mode = 0;
  Turn = 0;
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  Pause3();
  while(1){
    if(Bump_Read()){ // collision
      // turn 180 if bumped
      Motor_Stop();

      Motor_Backward(5000,5000);
      Clock_Delay1ms(100);

      Motor_Right(5000, 5000);
      Clock_Delay1ms(800);
      Mode = 1;
    }
    if(ControllerFlag){ // 100 Hz , not real time
      LCDOut3();
      ControllerFlag = 0;
    }
  }
}

int main(void){
//    Program17_1();  // drive straight using tachometer, added functionality to get though maze using bump for level 3
  Program17_2();  // drive straight using IR sensors, added functionality to get through maze for level 4
//    Program17_3();  // drive on a white line with black background, added functionality to reverse for level 1
//    Level_2();  // uses line follow from Program17_3() but recognises turns to get through maze for level 2
}