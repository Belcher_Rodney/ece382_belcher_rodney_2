; LEDs.asm
; Runs on MSP432
; Capt Steven Beyer
; September 9, 2019


;	Code to activate LED on P5.4. This code accompanies the Lab08_LED_Switchesmain.c
;
       .thumb
       .text
       .align 2
       .global LED_Init
       .global LED_Off
       .global LED_On
       .global LED_Toggle
       .global LED_Oscillate

; function to initialize P5.4
LED_Init:	.asmfunc
	LDR R1, P5SEL0
	LDRB R0, [R1]
	BIC R0, R0, #0x10	; GPIO
	STRB R0, [R1]
	LDR R1, P5SEL1
	LDRB R0, [R1]
	BIC R0, R0, #0x10
	STRB R0, [R1]		; GPIO
	LDR R1, P5DIR
	LDRB R0, [R1]
	ORR R0, R0, #0x10	; output
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to turn off P5.4
LED_Off:		.asmfunc
	LDR R1, P5OUT
	LDRB R0, [R1]		; 8-bit read
	BIC R0, R0, #0x10	; turn off
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to turn on P5.4
LED_On:	.asmfunc
	LDR R1, P5OUT
	LDRB R0, [R1]		; 8-bit read
	ORR R0, R0, #0x10	; turn on
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to toggle P5.4
LED_Toggle: .asmfunc
	LDR R1, P5OUT
	LDRB R0, [R1]		; 8-bit read
	EOR R0, R0, #0x10	; toggle
	STRB R0, [R1]
	BX LR
	.endasmfunc

; function to continuously toggle P5.4 every half second
; use a loop as a timer
LED_Oscillate:	.asmfunc

loop		BL LED_Toggle
			LDR R2, DELAY
decrement	SUB R2, #0x01
			CMP R2, #0x00
			BNE decrement
			b loop
			.endasmfunc

; addresses for Port 5 registers
	.align 4
P5SEL0 .field 0x40004C4A,32
P5SEL1 .field 0x40004C4C,32
P5DIR  .field 0x40004C44,32
P5OUT  .field 0x40004C42,32
DELAY  .field 4000000,32
	.end
